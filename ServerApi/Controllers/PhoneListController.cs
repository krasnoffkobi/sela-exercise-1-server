using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace ServerApi.Controllers
{
    [Route("api/phonelist")]
    [ApiController]
    public class PhoneListController : ControllerBase
    {
        private static DAL.DBContext context = new DAL.DBContext();
        
        [HttpGet]
        [Route("getPhoneListCSV")]
        public ActionResult<List<DAL.PhoneRecord>> GetCSV()
        {
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            StringBuilder res = new StringBuilder();

            res.Append("id,First Name,Last Name,Phone\r\n");

            foreach(var el in mPhoneBook.getPhoneList(ref context)) {
                res.Append(el.id);
                res.Append(",");
                res.Append(el.FirstName);
                res.Append(",");
                res.Append(el.LastName);
                res.Append(",");
                res.Append(el.PhoneNumber);
                res.Append("\r\n");
            }

            var result = new FileContentResult(Encoding.UTF8.GetBytes(res.ToString()), "application/octet-stream");
            result.FileDownloadName = "my-csv-file.csv";
            return result;
        }

        [HttpPost]
        [Route("uploadFile")]
        public IActionResult UploadFile([FromBody]DAL.UploadFile request)
        {
            DAL.PhoneRecord rec;
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            
            byte[] data = Convert.FromBase64String(request.fileData.Split(",")[1]);
            string decodedString = Encoding.UTF8.GetString(data);

            string[] decodedStringArr = decodedString.Split("\r\n");
            foreach (string el in decodedStringArr) {
                if (!string.IsNullOrEmpty(el) && el != "id,First Name,Last Name,Phone") {
                    string[] cols = el.Split(",");
                    rec = new DAL.PhoneRecord();
                    rec.id = Guid.Parse(cols[0]);
                    rec.FirstName = cols[1];
                    rec.LastName = cols[2];
                    rec.PhoneNumber = cols[3];
                    mPhoneBook.AddNewPhone(ref context, rec);
                }
            }

            return Ok("[]");
        }
        
        [HttpGet]
        [Route("getPhoneList")]
        public ActionResult<List<DAL.PhoneRecord>> Get()
        {
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            return mPhoneBook.getPhoneList(ref context);
        }

        [HttpPost]
        [Route("addNewPhone")]
        public IActionResult Post([FromBody]DAL.PhoneRecord request)
        {
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            request.id= Guid.NewGuid();
            mPhoneBook.AddNewPhone(ref context, request);
            return Ok("[]");
        }

        [HttpDelete("deletePhone/{id}")]
        public IActionResult Delete(string id)
        {
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            mPhoneBook.deletePhone(ref context, id);
            return Ok("[]");
        }

        [HttpPut("EditPhone/{id}")]
        public IActionResult EditPhone(string id, [FromBody]DAL.PhoneRecord request)
        {
            BLL.PhoneBook mPhoneBook = new BLL.PhoneBook();
            request.id = Guid.Parse(id);
            mPhoneBook.EditPhoneList(ref context, request);
            return Ok("[]");
        }
    }
}
