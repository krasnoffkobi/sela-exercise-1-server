using System;

namespace DAL
{
    public class PhoneRecord
    {
        public Guid id { get; set; }
        public string FirstName { get; set;}
        public string LastName { get; set;}
        public string PhoneNumber { get; set;}
    }
}