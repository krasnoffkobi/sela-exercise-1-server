﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class PhoneBook
    {
        public void AddNewPhone(ref DAL.DBContext context, DAL.PhoneRecord record)
        {
            context.PhoneList.Add(record);
        }

        public List<DAL.PhoneRecord> getPhoneList(ref DAL.DBContext context)
        {
            return (context.PhoneList.AsEnumerable().OrderBy(el => el.LastName + " " + el.FirstName)).ToList();
        }

        public List<DAL.PhoneRecord> getPhoneList(ref DAL.DBContext context, DAL.PhoneRecord record)
        {
            return context.PhoneList.Where(w => w.id == record.id).ToList();
        }

        public void EditPhoneList(ref DAL.DBContext context, DAL.PhoneRecord record)
        {
            context.PhoneList.Where(w => w.id == record.id).ToList().ForEach(el => {
                el.FirstName = record.FirstName;
                el.LastName = record.LastName;
                el.PhoneNumber = record.PhoneNumber;
            });
        }

        public void deletePhone(ref DAL.DBContext context, string id)
        {
            var itemToRemove = context.PhoneList.SingleOrDefault(r => r.id.ToString() == id);
            if (itemToRemove != null)
                context.PhoneList.Remove(itemToRemove);
        }
    }
}
